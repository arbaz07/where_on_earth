

const popup = document.querySelector(".popup-container");
const containerDiv = document.querySelector(".container");
const apiurl = "https://restcountries.com/v3.1/all";



fetch(apiurl)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    loaderBlocker();
    createBlock(data);

    containerDiv.addEventListener("click", (e) => {
      popup.replaceChildren('');
      createPopup(e, data);
      // showPopup();
      
    });

  })
  .then(()=>{
    
  })
  .catch((err) => {
    loaderBlocker();
    error();
    console.log(err);
  });



  function pop(id) {
    return document.getElementById(id);
  }
  
  function show(id) {
    pop(id).style.display ='flex';
    window.onclick = (event)=>{
      if(event.target.matches('.popup-container')){
        hide(id)
      }
    }
    
  }
  function hide(id) {
    pop(id).style.display ='none';
  }
  // popup.addEventListener("click" ,(clickEvent)=>{
  //   clickEvent.stopPropagation();
  // })



function createBlock(data) {

  data.forEach((item) => {
    const imageLink = item.flags.png;
    const countryName = item.name.official;
    const population = item.population;
    const region = item.region;
    const capital = item.capital;

    let div = document.createElement("div");
    div.className = "country box";
    // div.id = 'box';

    // console.log(div);

    let imageTag = document.createElement("img");
    imageTag.src = imageLink;
      
    imageTag.className = "country-img-top";
    div.append(imageTag);

    // console.log(div)

    let innerDiv = document.createElement("div");
    innerDiv.className = "country-body";

    let h2Tag = document.createElement("h2");
    h2Tag.className = "country-title";
    let h2Text = document.createTextNode(countryName.toUpperCase());
    h2Tag.appendChild(h2Text);

    let pPopulationTag = document.createElement("p");
    pPopulationTag.className = "country-text";
    let pPopulationTagText = document.createTextNode(
      "Population:-" + population
    );
    pPopulationTag.appendChild(pPopulationTagText);

    let pRegionTag = document.createElement("p");
    pRegionTag.className = "country-text";
    pRegionTag.id = "regionTag";
    let pRegionTagText = document.createTextNode("Region:-" + region);
    pRegionTag.appendChild(pRegionTagText);

    let pCapitalTag = document.createElement("p");
    pCapitalTag.className = "country-text";
    let pCapitalTagText = document.createTextNode("Capital:-" + capital);
    pCapitalTag.appendChild(pCapitalTagText);

    // cardBody.appendChild(imageTag);
    innerDiv.appendChild(h2Tag);
    innerDiv.appendChild(pPopulationTag);
    innerDiv.appendChild(pRegionTag);
    innerDiv.appendChild(pCapitalTag);

    div.appendChild(innerDiv);

    containerDiv.appendChild(div);
  });
}


















function createPopup(e, data) {
  //path veriable is depricated so used e.composed path
  let path = e.composedPath();
  const selectedCountry = path[path.length - 7].children[1].firstChild.textContent.toUpperCase();
   
  data.forEach((country) => {
    const currentCountryName = country.name.official.toUpperCase();

    if (selectedCountry == currentCountryName) {
      let imageTag = document.createElement("img");
      imageTag.src = country.flags.png;
      imageTag.id = "popup-image";
      // console.log(image);

      let popupDiv = document.createElement("div");
      popupDiv.className = "popup-container-right";

      // let button = document.createElement('button');
      // button.id="button";
      // button.onclick=`hide('popup')`;
      // let button_text = button.createTextNode='Okey';
      // button.append(button_text);

      let h1 = document.createElement("h1");
      let h1_text = (document.createTextNode =
        country.name.common.toUpperCase());
        
      h1.append(h1_text);
      popupDiv.appendChild(h1);

      // console.log(popup)

      let innerPopupDiv = document.createElement("div");
      innerPopupDiv.className = "country-details";

      let p1 = document.createElement("p");
      let p1_text = (document.createTextNode =
        "Native Name :- " +
        Object.values(Object.values(country.name.nativeName)[0]).toString());
      p1.append(p1_text);
      let p2 = document.createElement("p");
      let p2_text = (document.createTextNode =
        "Population - " + country.population);
      p2.append(p2_text);
      let p3 = document.createElement("p");
      let p3_text = (document.createTextNode = "Top Level Domain :- be");
      p3.append(p3_text);
      let p4 = document.createElement("p");
      let p4_text = (document.createTextNode =
        "Currencies :- " + Object.keys(country.currencies)[0]);
      p4.append(p4_text);
      let p5 = document.createElement("p");
      let p5_text = (document.createTextNode = "Region :- " + country.region);
      p5.append(p5_text);
      let p6 = document.createElement("p");
      let p6_text = (document.createTextNode =
        "Languages :- " + Object.values(country.languages).toString());
      p6.append(p6_text);
      let p7 = document.createElement("p");
      let p7_text = (document.createTextNode =
        "Sub Region :- " + country.subregion);
      p7.append(p7_text);
      let p8 = document.createElement("p");
      let p8_text = (document.createTextNode =
        "Capital :- " + country.capital.toString());
      p8.append(p8_text);

      let cancelButton = document.createElement("button");
      cancelButton.setAttribute("id","cancel-button");
      cancelButton.onclick = ()=>{
        hide('popup');
      };
      cancelButton.innerHTML = "back";

      // let btn = document.createElement("p");
      // btn_text = document.createTextNode='back';
      // btn.append(btn);

      innerPopupDiv.appendChild(p1);
      innerPopupDiv.appendChild(p2);
      innerPopupDiv.appendChild(p3);
      innerPopupDiv.appendChild(p4);
      innerPopupDiv.appendChild(p5);
      innerPopupDiv.appendChild(p6);
      innerPopupDiv.appendChild(p7);
      innerPopupDiv.appendChild(p8);
      innerPopupDiv.appendChild(cancelButton);
      // console.log(innerPopupDiv);

      let borderCountriesDiv = document.createElement("div");
      borderCountriesDiv.className = "border-countries";

      let h4 = document.createElement("h4");
      let h4_text = (document.createTextNode = "Border Country :- ");
      h4.append(h4_text);

      if (country.borders != null) {
        Object.values(country.borders).forEach((item) => {
          let span = document.createElement("span");
          // console.log(item);
          let span_text = (document.createTextNode =item);
          span.append(span_text);
          borderCountriesDiv.appendChild(span);
        });
      }

      borderCountriesDiv.appendChild(h4);


      // let button = document.createElement('h1');
      // button.appendChild(document.createTextNode='Back');
      // button.attributes(`onclick`,`hide('popup')`);


      popupDiv.appendChild(innerPopupDiv);
      popupDiv.appendChild(borderCountriesDiv);
      // popupDiv.appendChild(button);
      // let btn = document.getElementById('back-btn');
      // popup.insertBefore(imageTag,btn);
      // popup.insertBefore(popupDiv,btn);
      popup.appendChild(imageTag);
      popup.appendChild(popupDiv);
      // popup.appendChild(button);
    }
  });
}






function loaderBlocker() {
  const loader = document.querySelector(".center");
  loader.remove();
}






function error() {
  let errorBox = document.querySelector(".errorBox");
  errorBox.style.display = "gird";
  let header = document.getElementById("main-header");
  header.style.display = "none";
}







//escape key
document.onkeydown =(event)=> {
  let isEscape = false;
  // console.log(e);
  if ("key" in event) {
    isEscape =( event.key === "Escape" || event.key === "Esc");
  }
  if (isEscape) {
    hide('popup')

  }
};




























const countries = document.querySelector(".container");
const filter = document.getElementById("filter");
filter.addEventListener("keyup", filterCountries);

function filterCountries(event) {
  let keyWords = event.target.value.toUpperCase();

  let countryDiv = countries.getElementsByClassName("country");

  Array.from(countryDiv).forEach((country) => {
    let h2 = country.getElementsByTagName("h2");
    let countryName = h2[0].textContent.toUpperCase();

    if (countryName.includes(keyWords)) {
      country.style.display = "block";
    } else {
      country.style.display = "none";
    }
  });
}








const select = document.getElementById("regionId");
select.addEventListener("change", filterByRegion);

function filterByRegion(event) {
  let selectedRegion = event.target.value.toUpperCase();

  let countryDiv = countries.getElementsByClassName("country");

  Array.from(countryDiv).forEach((country) => {
    // console.log(country);
    let p = country.querySelector(".country>.country-body>#regionTag");

    let regionName = p.innerHTML.toUpperCase();
    // console.log(regionName)

    if (regionName.includes(selectedRegion)) {
      country.style.display = "block";
    } else {
      country.style.display = "none";
    }
  });
}






const darkModeButton = document.getElementById("btn");
darkModeButton.addEventListener("click", makeDark);
let clickCount = 1;
function makeDark() {
  let body = document.getElementsByTagName("body");
  let label = document.querySelectorAll(
    `header,label,h1,select,p,h2,.popup-container,
    .popup-container h1,.popup-container span,
    .country,button`
  );

  // let containerAndHeader= document.querySelectorAll('header,.container');

  let modeText;
  if (clickCount % 2 != 0) {
    modeText = document.getElementById("btn").innerHTML = "Day Mode";
    body[0].style.backgroundColor = "black";

    Array.from(label).forEach((each) => {
      each.style.color = "white";
      each.style.backgroundColor = "rgb(69, 69, 69)";
      each.style.boxShadow = "none";
    });
  } else {
    modeText = document.getElementById("btn").innerHTML = "Dark Mode";
    body[0].style.backgroundColor = "white";

    Array.from(label).forEach((each) => {
      each.style.color = "rgb(51, 64, 105)";
      each.style.backgroundColor = " aliceblue";
    });
  }
  clickCount++;
}



