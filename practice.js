
  // TO MAKE THE MAP APPEAR YOU MUST
  // ADD YOUR ACCESS TOKEN FROM
  // https://account.mapbox.com
  mapboxgl.accessToken = 'pk.eyJ1IjoiYWpicmF1IiwiYSI6ImNraG5uZGttczA0b2wyenBqdnRrbnR0amcifQ.hvqQvd0wyIHbFxpWVVIEdw';
    const map = new mapboxgl.Map({
        container: 'map', // container ID
        // Choose from Mapbox's core styles, or make your own style with Mapbox Studio
        style: 'mapbox://styles/mapbox/streets-v11', // style URL
        center: [-74.5, 40], // starting position ([lng, lat] for Mombasa, Kenya)
        zoom: 9 // starting zoom
    });

    const positions = [
        [-74.5, 40],
        [-73.5, 41],
        [-72.5, 42],
        [-71.5, 43],
        [-70.5, 44],
        [-69.5, 45],
        [-68.5, 46],
        [-67.5, 47],
        [-66.5, 48],
        [-65.5, 49]
    ];

    map.on('load', function() {
        positions.forEach((position) => {
            new mapboxgl.Marker()
            .setLngLat(position)
            .addTo(map);
        });
    });

    /*
     *  When a user clicks the button, `fitBounds()` zooms and pans
     *  the viewport to contain a bounding box that surrounds Kenya.
     *  The [lng, lat] pairs are the southwestern and northeastern
     *  corners of the specified geographical bounds.
     */
    document.getElementById('fit').addEventListener('click', () => {
        map.fitBounds([
            [-74.5, 40], // southwestern corner of the bounds
            [-65.5, 49]// northeastern corner of the bounds
        ]);
    });





// function fetchData(){
//   fetch('http://api.open-notify.org/iss-now.json')
//   .then((response)=>{
//     return response.json();
//   })
//   .then((data)=>{
//     // let totalPopulation = data.reduce((acc, item)=>{
//     //   return acc+=item.population;
//     // },0);
//     console.log(data);
//     console.timeEnd()
//   })

// }
// console.time()
// fetchData();